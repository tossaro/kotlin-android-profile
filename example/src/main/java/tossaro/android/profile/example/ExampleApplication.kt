package tossaro.android.profile.example

import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.module.dsl.bind
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module
import tossaro.android.core.CoreApplication
import tossaro.android.core.external.utility.NetworkUtil
import tossaro.android.profile.app.profile.ProfileViewModel
import tossaro.android.profile.app.editprofile.EditProfileViewModel
import tossaro.android.profile.app.signout.SignOutDialogViewModel
import tossaro.android.profile.data.user.UserRepositoryImpl
import tossaro.android.profile.data.user.network.UserServiceV1
import tossaro.android.profile.domain.user.UserRepository
import tossaro.android.profile.domain.user.usecase.GetProfileUseCase
import tossaro.android.profile.domain.user.usecase.SignOutUseCase

class ExampleApplication : CoreApplication() {

    override fun koinModule() = module {
        single { NetworkUtil.buildService<UserServiceV1>(BuildConfig.SERVER_V1, get()) }

        singleOf(::UserRepositoryImpl) { bind<UserRepository>() }
        singleOf(::GetProfileUseCase)
        singleOf(::SignOutUseCase)

        viewModelOf(::SignOutDialogViewModel)
        viewModelOf(::ProfileViewModel)
        viewModelOf(::EditProfileViewModel)
    }

}