package tossaro.android.profile.data.user.network

import com.haroldadmin.cnradapter.NetworkResponse
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Header
import tossaro.android.core.data.network.response.ApiResponse
import tossaro.android.profile.domain.user.entity.User

interface UserServiceV1 {
    @GET("/user/api/profile")
    suspend fun getProfile(@Header("Authorization") token: String?): NetworkResponse<ApiResponse<User>, ApiResponse<Void>>

    @DELETE("/user/api/signout")
    suspend fun signOut(@Header("Authorization") token: String?): NetworkResponse<ApiResponse<Void>, ApiResponse<Void>>
}