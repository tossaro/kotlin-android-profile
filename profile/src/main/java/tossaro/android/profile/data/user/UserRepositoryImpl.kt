package tossaro.android.profile.data.user

import android.content.SharedPreferences
import com.haroldadmin.cnradapter.NetworkResponse
import tossaro.android.core.data.network.response.ApiResponse
import tossaro.android.core.external.constant.AppConstant
import tossaro.android.profile.data.user.network.UserServiceV1
import tossaro.android.profile.domain.user.UserRepository
import tossaro.android.profile.domain.user.entity.User

class UserRepositoryImpl(
    private val sharedPreferences: SharedPreferences,
    private val userServiceV1: UserServiceV1
) : UserRepository {
    override suspend fun getProfile(): NetworkResponse<ApiResponse<User>, ApiResponse<Void>> {
        val accessToken = sharedPreferences.getString(AppConstant.ACCESS_TOKEN, null)
        return userServiceV1.getProfile("Bearer " + accessToken)
    }

    override suspend fun signOut(): NetworkResponse<ApiResponse<Void>, ApiResponse<Void>> {
        val accessToken = sharedPreferences.getString(AppConstant.ACCESS_TOKEN, null)
        return userServiceV1.signOut("Bearer " + accessToken)
    }
}