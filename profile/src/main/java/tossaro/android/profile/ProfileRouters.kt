package tossaro.android.profile

object ProfileRouters {
    const val EDIT_PROFILE = "/edit-profile"
    const val SIGN_OUT = "/sign-out"
}