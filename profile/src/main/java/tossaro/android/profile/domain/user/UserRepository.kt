package tossaro.android.profile.domain.user

import com.haroldadmin.cnradapter.NetworkResponse
import tossaro.android.core.data.network.response.ApiResponse
import tossaro.android.profile.domain.user.entity.User

interface UserRepository {
    suspend fun getProfile(): NetworkResponse<ApiResponse<User>, ApiResponse<Void>>
    suspend fun signOut(): NetworkResponse<ApiResponse<Void>, ApiResponse<Void>>
}