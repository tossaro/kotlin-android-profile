package tossaro.android.profile.domain.user.entity

data class User(
    val id: Int,
    val name: String,
    val email: String,
    val phone: String,
    val bio: String?
)