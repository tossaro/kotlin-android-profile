package tossaro.android.profile.domain.user.usecase

import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import tossaro.android.profile.domain.user.UserRepository

class SignOutUseCase : KoinComponent {
    private val userRepository: UserRepository by inject()
    suspend operator fun invoke() = userRepository.signOut()
}