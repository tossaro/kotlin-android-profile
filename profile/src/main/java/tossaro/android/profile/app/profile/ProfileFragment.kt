package tossaro.android.profile.app.profile

import android.os.Bundle
import android.view.View
import org.koin.androidx.viewmodel.ext.android.viewModel
import tossaro.android.core.app.BaseFragment
import tossaro.android.core.external.ext.goTo
import tossaro.android.profile.ProfileRouters
import tossaro.android.profile.R
import tossaro.android.profile.databinding.ProfileFragmentBinding


class ProfileFragment : BaseFragment<ProfileFragmentBinding>(R.layout.profile_fragment) {
    private val vm: ProfileViewModel by viewModel()

    override fun actionBarTitle() = getString(R.string.profile_title)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vm = vm
        binding.tvChangeProfile.setOnClickListener {
            goTo(ProfileRouters.EDIT_PROFILE)
        }
        binding.btnSignOut.setOnClickListener {
            goTo(ProfileRouters.SIGN_OUT)
        }
    }
}