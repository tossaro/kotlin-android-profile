package tossaro.android.profile.app.signout

import android.content.Intent
import android.os.Bundle
import android.view.View
import org.koin.androidx.viewmodel.ext.android.viewModel
import tossaro.android.core.app.BaseDialogFragment
import tossaro.android.profile.R
import tossaro.android.profile.databinding.SignoutDialogFragmentBinding


class SignOutDialogFragment :
    BaseDialogFragment<SignoutDialogFragmentBinding>(R.layout.signout_dialog_fragment) {
    private val vm: SignOutDialogViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vm = vm.also {
            it.onSignOut.observe(viewLifecycleOwner, ::onSignOut)
            it.onCancel.observe(viewLifecycleOwner, ::cancel)
        }
    }

    private fun onSignOut(boolean: Boolean) {
        requireActivity().finish()
        startActivity(Intent(requireContext(), requireActivity()::class.java))
    }

    private fun cancel(boolean: Boolean) {
        dismiss()
    }
}