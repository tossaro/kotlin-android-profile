package tossaro.android.profile.app.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.haroldadmin.cnradapter.NetworkResponse
import kotlinx.coroutines.launch
import tossaro.android.core.app.BaseViewModel
import tossaro.android.profile.domain.user.entity.User
import tossaro.android.profile.domain.user.usecase.GetProfileUseCase

class ProfileViewModel(
    private val getProfileUseCase: GetProfileUseCase,
) : BaseViewModel() {
    val user = MutableLiveData<User?>()

    init {
        getProfile()
    }

    fun getProfile() {
        // remove below 2 line of code if integration to api
        user.value = User(
            1,
            "Oliver Scott",
            "oliver.scott@email.com",
            "+62811123123",
            "Akulah Seorang Petualang"
        )
        return
        viewModelScope.launch {
            loadingIndicator.value = true
            when (val resp = getProfileUseCase()) {
                is NetworkResponse.Success -> {
                    user.value = resp.body.data
                    loadingIndicator.value = false
                }
                is NetworkResponse.ServerError -> {
                    loadingIndicator.value = false
                    alertMessage.value = resp.body?.meta?.message.toString()
                }
                is NetworkResponse.NetworkError -> {
                    loadingIndicator.value = false
                    alertMessage.value = resp.error.message.orEmpty()
                }
            }
        }
    }
}