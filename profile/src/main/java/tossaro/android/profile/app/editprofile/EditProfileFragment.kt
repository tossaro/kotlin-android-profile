package tossaro.android.profile.app.editprofile

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import org.koin.androidx.viewmodel.ext.android.viewModel
import tossaro.android.core.app.BaseFragment
import tossaro.android.profile.R
import tossaro.android.profile.databinding.EditProfileFragmentBinding

class EditProfileFragment :
    BaseFragment<EditProfileFragmentBinding>(R.layout.edit_profile_fragment) {
    private val vm: EditProfileViewModel by viewModel()

    override fun actionBarTitle() = getString(R.string.edit_profile_title)
    override fun isBottomNavBarShown() = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vm = vm
        binding.btnSave.setOnClickListener {
            findNavController().navigateUp()
        }
    }
}