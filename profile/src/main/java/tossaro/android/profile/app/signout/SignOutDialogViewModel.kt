package tossaro.android.profile.app.signout

import android.content.SharedPreferences
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.haroldadmin.cnradapter.NetworkResponse
import kotlinx.coroutines.launch
import tossaro.android.core.app.BaseViewModel
import tossaro.android.core.external.constant.AppConstant
import tossaro.android.profile.domain.user.usecase.SignOutUseCase

class SignOutDialogViewModel(
    private val sharedPreferences: SharedPreferences,
    private val signOutUseCase: SignOutUseCase,
) : BaseViewModel() {
    val onSignOut = MutableLiveData<Boolean>()
    val onCancel = MutableLiveData<Boolean>()

    fun signOut() {
        // remove below 3 line of code if integration to api
        sharedPreferences.edit().remove(AppConstant.ACCESS_TOKEN).remove(AppConstant.REFRESH_TOKEN)
            .apply()
        onSignOut.value = true
        return
        viewModelScope.launch {
            loadingIndicator.value = true
            when (val resp = signOutUseCase()) {
                is NetworkResponse.Success -> {
                    sharedPreferences.edit().remove(AppConstant.ACCESS_TOKEN)
                        .remove(AppConstant.REFRESH_TOKEN).apply()
                    onSignOut.value = true
                    loadingIndicator.value = false
                }
                is NetworkResponse.ServerError -> {
                    loadingIndicator.value = false
                    alertMessage.value = resp.body?.meta?.message.toString()
                }
                is NetworkResponse.NetworkError -> {
                    loadingIndicator.value = false
                    alertMessage.value = resp.error.message.orEmpty()
                }
            }
        }
    }

    fun cancel() {
        onCancel.value = true
    }
}